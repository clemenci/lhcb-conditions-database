#!/usr/bin/env python3
from bisect import bisect_left
from collections.abc import Iterable, Iterator
from itertools import groupby
from os.path import relpath
from pathlib import Path


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser(
        description="Reorganize directories with many IOVs grouping files in subdirectories"
    )
    parser.add_argument(
        "path",
        nargs="*",
        help="directories to process (if missing, start from the current directory)",
        type=Path,
    )

    args = parser.parse_args()
    if not args.path:
        args.path = [Path()]

    for path in gather_paths(args.path):
        process_directory(path)


def gather_paths(paths: Iterable[Path]) -> Iterator[Path]:
    """
    Recurse into the specified paths looking for directories containing the
    flag file '.condition' and yield each of them one by one.
    """
    to_recurse: list[Path] = []
    for path in paths:
        if path.is_dir():
            if (path / ".condition").exists():
                yield path
            else:
                to_recurse.extend(p for p in path.iterdir() if p.is_dir())
        else:
            raise ValueError(f"invalid path {path}: not a directory")
    if to_recurse:
        yield from gather_paths(to_recurse)


def process_directory(path: Path):
    """
    Move all files and symlinks with numeric names into subdirectories to group
    them by thousands. E.g. the files 1025, 1040, 2051 will go respectively
    into the subdirectories 1000, 1000 and 2000.
    """
    # collect all numeric files (or symlinks pointing to files)
    entries = sorted(
        (int(p.name), p)
        for p in path.iterdir()
        if str(p.name).isdigit() and p.is_file()
    )
    groups = list(
        (key, list(group))
        for key, group in groupby(
            entries, key=lambda element: element[0] // 1000 * 1000
        )
    )[:-1]
    for key, group in groups:
        dst_dir = path / str(key)
        # we move the files if there is more than one file or the target
        # directory already exist
        if len(group) > 1 or dst_dir.is_dir():
            if dst_dir.is_file():
                # we have a file matching the directory we want to create,
                # let's move it aside for a moment
                dst_dir.rename(dst_dir.with_name(dst_dir.name + ".tmp"))
            dst_dir.mkdir(exist_ok=True)
            (dst_dir / ".condition").touch(exist_ok=True)
            for _, entry in group:
                dst = dst_dir / entry.name
                if entry.name == dst_dir.name:
                    # this is the file we temporarily moved aside
                    entry = entry.with_name(entry.name + ".tmp")
                if entry.is_symlink():
                    dst.symlink_to(relpath(entry.resolve(), dst_dir.resolve()))
                    entry.unlink()
                else:
                    entry.rename(dst)
    close_gaps(path)


def close_gaps(condition: Path):
    """
    For each IOV group in the given condition directory make sure there
    is a first conditions with the same IOV start as the group.
    If it doesn't already exist, add a symlink to the last condition in
    the previous group.
    """

    def get_iov_entries(path: Path):
        entries = [(int(p.name), p) for p in path.iterdir() if str(p.name).isdigit()]
        entries.sort()
        return entries

    all_paths = get_iov_entries(condition)

    def find_previous(group):
        i = bisect_left(all_paths, (int(group.name),))
        return all_paths[i - 1][1] if i else None

    groups = [entry for entry in all_paths if entry[1].is_dir()]
    for _, group in groups:
        if not (group / group.name).exists():
            previous = find_previous(group)
            if previous.is_dir():
                last = get_iov_entries(previous)[-1][1]
                (group / group.name).symlink_to(Path("..") / previous.name / last.name)


if __name__ == "__main__":
    main()
