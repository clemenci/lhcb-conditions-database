#!/bin/bash -e

# switch to repository root directory
cd $(dirname $0)/..
# tell git not to look for the repository
export GIT_WORK_TREE=$(pwd)
export GIT_DIR=${GIT_WORK_TREE}/.git

ssh-add /home/online/.ssh/lbconddb_rsa

# loop forever to keep the repository in sync
while true ; do
    if [ ! -e no-sync ] ; then
        # make sure all files are considered
        git add .
        # commit all changes ('git commit --dry-run' checks if there is something to commit)
        git commit --dry-run --short --quiet && git commit -m "online conditions flush" --no-verify --no-status --quiet
        # synchronize with remote changes (keeping our version of conflicting changes)
        git pull -X ours --no-edit --quiet
        git push --quiet 2>&1 | grep -v ^remote:
    fi
    # wait a bit between synchronizations
    sleep 600
done
