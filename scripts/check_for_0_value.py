#!/usr/bin/env python3
"""
Check that all condition top level directories have a payload for
the time point 0.
"""
import os
from collections.abc import Generator


def find_missing_0s(top: str) -> Generator[str, None, None]:
    """
    Scan the tree from `top` to find condition directories
    and return a generator of those that do not have a payload
    for time point 0.
    """
    for root, dirs, files in os.walk(top):
        if ".condition" in files:
            # root is a condition directory
            if "0" not in files and "0" not in dirs:
                # no payload for 0, this is bad
                yield root
            # no need to discend further
            dirs[:] = []


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser(description=__doc__)
    parser.add_argument("top", help="where to start the scan from")

    args = parser.parse_args()

    bad_dirs = list(find_missing_0s(args.top))
    if bad_dirs:
        bad_dirs.sort()
        print("missing payload for time point 0 in:", *bad_dirs, sep="\n  ")
        exit(1)


if __name__ == "__main__":
    main()
